
<!-- README.md is generated from README.Rmd. Please edit that file -->

# msr

<!-- badges: start -->
<!-- badges: end -->

West and Central Africa Monthly Statistical Report

## Installation

You can install the development version of msr like so:

``` r
remotes::install_gitlab("dickoa/msr")
```

## Example

This is a basic example which shows you how to solve a common problem:

``` r
library(msr)
## basic example code
```
